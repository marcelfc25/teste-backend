const dotenv = require('dotenv');
const pg = require('pg');

dotenv.config();

const pool = new pg.Pool({
  user: process.env.POSTGRES_USERNAME,
  host: process.env.POSTGRES_HOST,
  database: process.env.POSTGRES_DATABASE,
  password: process.env.POSTGRES_PASSWORD,
  port: process.env.POSTGRES_PORT,
});
pool.query("INSERT INTO users(name, email, password, role) VALUES ('Admin', 'admin@admin.com', '$2a$08$/28/Ns6sVdQHn5vTzDg64OiB2z8csdySbkpyK/vUSw0oLbfm03yMm', 'admin')", (err, res) => {
  console.log(err, res);
  pool.end();
});
