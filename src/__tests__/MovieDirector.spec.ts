import request from 'supertest';
import { getConnection } from 'typeorm';

import UsersRepository from '../modules/users/infra/typeorm/repositories/UsersRepository';
import CreateAdminService from '../modules/users/services/CreateAdminService';
import app from '../shared/infra/http/app';
import createConnection from '../shared/infra/typeorm/index';

describe('MovieDirector', () => {
  beforeAll(async () => {
    const connection = await createConnection();
    await connection.runMigrations();
  });

  afterAll(async () => {
    const connection = getConnection();
    await connection.dropDatabase();
    await connection.close();
  });

  it('should be able to relate a actor to a movie', async () => {
    const usersRepository = new UsersRepository();
    const createAdmin = new CreateAdminService(usersRepository);
    await createAdmin.execute({
      name: 'user admin',
      email: 'admin3@admin.com',
      password: '123456',
    });

    const response1 = await request(app).post('/sessions')
      .send({
        email: 'admin3@admin.com',
        password: '123456',
      });

    const response2 = await request(app).post('/movies')
      .send({
        title: 'Homem de Ferro',
        original_title: 'Iron Man',
        duration: 206,
        rating: 12,
        release_year: 2008,
      })
      .set('Authorization', `Bearer ${response1.body.token}`);

    const response3 = await request(app).post('/people')
      .send({
        name: 'Robert Downey Jr.',
        age: 55,
      })
      .set('Authorization', `Bearer ${response1.body.token}`);

    const response4 = await request(app).post('/movies/movieDirector')
      .send({
        movie_id: response2.body.id,
        person_id: response3.body.id,
      })
      .set('Authorization', `Bearer ${response1.body.token}`);

    expect(response4.status).toBe(200);
    expect(response4.body).toEqual(
      expect.objectContaining({
        movie_id: response2.body.id,
        person_id: response3.body.id,
      }),
    );
  });
});
