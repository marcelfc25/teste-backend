import request from 'supertest';
import { getConnection } from 'typeorm';

import UsersRepository from '../modules/users/infra/typeorm/repositories/UsersRepository';
import CreateAdminService from '../modules/users/services/CreateAdminService';
import app from '../shared/infra/http/app';
import createConnection from '../shared/infra/typeorm/index';

describe('Movies', () => {
  beforeAll(async () => {
    const connection = await createConnection();
    await connection.runMigrations();
  });

  afterAll(async () => {
    const connection = getConnection();
    await connection.dropDatabase();
    await connection.close();
  });

  it('should be able to filter all movies', async () => {
    const response = await request(app).get('/movies');
    expect(response.status).toBe(200);
  });

  it('should be able to filter all movies by title', async () => {
    const usersRepository = new UsersRepository();
    const createAdmin = new CreateAdminService(usersRepository);
    await createAdmin.execute({
      name: 'user admin',
      email: 'adminfindtitle@admin.com',
      password: '123456',
    });

    const response1 = await request(app).post('/sessions')
      .send({
        email: 'adminfindtitle@admin.com',
        password: '123456',
      });

    const response2 = await request(app).post('/movies')
      .send({
        title: 'Homem de Ferro',
        original_title: 'Iron Man',
        duration: 206,
        rating: 12,
        release_year: 2008,
      })
      .set('Authorization', `Bearer ${response1.body.token}`);

    const response = await request(app).get('/movies?title=homem');
    expect(response.status).toBe(200);
    expect(response.body).toHaveLength(1);
    expect(response.body).toEqual(
      expect.arrayContaining([{
        id: response2.body.id,
        title: 'Homem de Ferro',
        original_title: 'Iron Man',
        duration: 206,
        rating: 12,
        release_year: 2008,
        actors: [],
        directors: [],
        genres: [],
        votes: [],
        average_votes: null,
      }]),
    );
  });

  it('should be able to filter all movies by genre', async () => {
    const usersRepository = new UsersRepository();
    const createAdmin = new CreateAdminService(usersRepository);
    await createAdmin.execute({
      name: 'user admin',
      email: 'adminfindgenre@admin.com',
      password: '123456',
    });

    const response1 = await request(app).post('/sessions')
      .send({
        email: 'adminfindgenre@admin.com',
        password: '123456',
      });

    const response2 = await request(app).post('/movies')
      .send({
        title: 'Homem de Ferro',
        original_title: 'Iron Man',
        duration: 206,
        rating: 12,
        release_year: 2008,
      })
      .set('Authorization', `Bearer ${response1.body.token}`);

    const response3 = await request(app).post('/genres')
      .send({
        title: 'Comedy',
      })
      .set('Authorization', `Bearer ${response1.body.token}`);

    await request(app).post('/movies/movieGenre')
      .send({
        movie_id: response2.body.id,
        genre_id: response3.body.id,
      })
      .set('Authorization', `Bearer ${response1.body.token}`);

    const response = await request(app).get('/movies?genre=comedy');
    expect(response.status).toBe(200);
    expect(response.body).toHaveLength(1);
  });

  it('should be able to filter all movies by actor', async () => {
    const usersRepository = new UsersRepository();
    const createAdmin = new CreateAdminService(usersRepository);
    await createAdmin.execute({
      name: 'user admin',
      email: 'adminfindactor@admin.com',
      password: '123456',
    });

    const response1 = await request(app).post('/sessions')
      .send({
        email: 'adminfindactor@admin.com',
        password: '123456',
      });

    const response2 = await request(app).post('/movies')
      .send({
        title: 'Homem de Ferro',
        original_title: 'Iron Man',
        duration: 206,
        rating: 12,
        release_year: 2008,
      })
      .set('Authorization', `Bearer ${response1.body.token}`);

    const response3 = await request(app).post('/people')
      .send({
        name: 'Robert Downey Jr.',
        age: 55,
      })
      .set('Authorization', `Bearer ${response1.body.token}`);

    const response4 = await request(app).post('/movies/movieActor')
      .send({
        movie_id: response2.body.id,
        person_id: response3.body.id,
      })
      .set('Authorization', `Bearer ${response1.body.token}`);

    const response = await request(app).get('/movies?actor=Robert');
    expect(response.status).toBe(200);
    expect(response.body).toHaveLength(1);
  });

  it('should be able to filter all movies by director', async () => {
    const usersRepository = new UsersRepository();
    const createAdmin = new CreateAdminService(usersRepository);
    await createAdmin.execute({
      name: 'user admin',
      email: 'adminfinddirector@admin.com',
      password: '123456',
    });

    const response1 = await request(app).post('/sessions')
      .send({
        email: 'adminfinddirector@admin.com',
        password: '123456',
      });

    const response2 = await request(app).post('/movies')
      .send({
        title: 'Homem de Ferro',
        original_title: 'Iron Man',
        duration: 206,
        rating: 12,
        release_year: 2008,
      })
      .set('Authorization', `Bearer ${response1.body.token}`);

    const response3 = await request(app).post('/people')
      .send({
        name: 'Robert Downey Jr.',
        age: 55,
      })
      .set('Authorization', `Bearer ${response1.body.token}`);

    await request(app).post('/movies/movieDirector')
      .send({
        movie_id: response2.body.id,
        person_id: response3.body.id,
      })
      .set('Authorization', `Bearer ${response1.body.token}`);

    const response = await request(app).get('/movies?director=Robert');
    expect(response.status).toBe(200);
    expect(response.body).toHaveLength(1);
  });

  it('should be able to create a movie', async () => {
    const usersRepository = new UsersRepository();
    const createAdmin = new CreateAdminService(usersRepository);
    await createAdmin.execute({
      name: 'user admin',
      email: 'admin3@admin.com',
      password: '123456',
    });

    const response1 = await request(app).post('/sessions')
      .send({
        email: 'admin3@admin.com',
        password: '123456',
      });

    const response2 = await request(app).post('/movies')
      .send({
        title: 'Homem de Ferro',
        original_title: 'Iron Man',
        duration: 206,
        rating: 12,
        release_year: 2008,
      })
      .set('Authorization', `Bearer ${response1.body.token}`);

    expect(response2.status).toBe(200);
    expect(response2.body).toEqual(
      expect.objectContaining({
        title: 'Homem de Ferro',
        original_title: 'Iron Man',
        duration: 206,
        rating: 12,
        release_year: 2008,
      }),
    );
  });

  it('should be able to find a movie by id', async () => {
    const usersRepository = new UsersRepository();
    const createAdmin = new CreateAdminService(usersRepository);
    await createAdmin.execute({
      name: 'user admin',
      email: 'adminfindmovie@admin.com',
      password: '123456',
    });

    const response1 = await request(app).post('/sessions')
      .send({
        email: 'adminfindmovie@admin.com',
        password: '123456',
      });

    const response2 = await request(app).post('/movies')
      .send({
        title: 'Homem de Ferro',
        original_title: 'Iron Man',
        duration: 206,
        rating: 12,
        release_year: 2008,
      })
      .set('Authorization', `Bearer ${response1.body.token}`);

    const response3 = await request(app).get(`/movies/${response2.body.id}`);

    expect(response3.status).toBe(200);
    expect(response3.body).toEqual(
      expect.objectContaining({
        title: 'Homem de Ferro',
        original_title: 'Iron Man',
        duration: 206,
        rating: 12,
        release_year: 2008,
        actors: [],
        directors: [],
        genres: [],
      }),
    );
  });
});
