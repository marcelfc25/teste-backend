import request from 'supertest';
import { getConnection } from 'typeorm';

import UsersRepository from '../modules/users/infra/typeorm/repositories/UsersRepository';
import CreateAdminService from '../modules/users/services/CreateAdminService';
import app from '../shared/infra/http/app';
import createConnection from '../shared/infra/typeorm/index';

describe('Admin', () => {
  beforeAll(async () => {
    const connection = await createConnection();
    await connection.runMigrations();
  });

  afterAll(async () => {
    const connection = getConnection();
    await connection.dropDatabase();
    await connection.close();
  });

  it('should be able to create a new user admin', async () => {
    const usersRepository = new UsersRepository();
    const createAdmin = new CreateAdminService(usersRepository);
    await createAdmin.execute({
      name: 'user admin',
      email: 'admin@admin.com',
      password: '123456',
    });

    const response1 = await request(app).post('/sessions')
      .send({
        email: 'admin@admin.com',
        password: '123456',
      });

    const response2 = await request(app).post('/admin')
      .send({
        name: 'user 1',
        email: 'user1@gmail.com',
        password: '123456',
        password_confirmation: '123456',
      })
      .set('Authorization', `Bearer ${response1.body.token}`);

    expect(response2.status).toBe(200);
    expect(response2.body).toEqual(
      expect.objectContaining({
        name: 'user 1',
        email: 'user1@gmail.com',
      }),
    );
  });

  it('not should be able to create a new user with e-mail already existing', async () => {
    const usersRepository = new UsersRepository();
    const createAdmin = new CreateAdminService(usersRepository);
    await createAdmin.execute({
      name: 'user admin',
      email: 'admin2@admin.com',
      password: '123456',
    });

    const response1 = await request(app).post('/sessions')
      .send({
        email: 'admin2@admin.com',
        password: '123456',
      });

    await request(app).post('/admin')
      .send({
        name: 'user 1',
        email: 'user1@gmail.com',
        password: '123456',
        password_confirmation: '123456',
      })
      .set('Authorization', `Bearer ${response1.body.token}`);

    const response2 = await request(app).post('/admin')
      .send({
        name: 'user 2',
        email: 'user1@gmail.com',
        password: '123456',
        password_confirmation: '123456',
      })
      .set('Authorization', `Bearer ${response1.body.token}`);

    expect(response2.status).toBe(400);
  });

  it('should be able to update a user', async () => {
    const usersRepository = new UsersRepository();
    const createAdmin = new CreateAdminService(usersRepository);
    await createAdmin.execute({
      name: 'user admin',
      email: 'admin3@admin.com',
      password: '123456',
    });

    const response1 = await request(app).post('/sessions')
      .send({
        email: 'admin3@admin.com',
        password: '123456',
      });

    const response2 = await request(app).put(`/admin/${response1.body.user.id}`)
      .send({
        name: 'user 2',
        email: 'user4@gmail.com',
      })
      .set('Authorization', `Bearer ${response1.body.token}`);

    expect(response2.status).toBe(200);
    expect(response2.body).toEqual(
      expect.objectContaining({
        name: 'user 2',
        email: 'user4@gmail.com',
      }),
    );
  });

  it('should be able to delete a user', async () => {
    const usersRepository = new UsersRepository();
    const createAdmin = new CreateAdminService(usersRepository);
    await createAdmin.execute({
      name: 'user admin',
      email: 'admin4@admin.com',
      password: '123456',
    });

    const response1 = await request(app).post('/sessions')
      .send({
        email: 'admin4@admin.com',
        password: '123456',
      });

    const response2 = await request(app).delete(`/users/${response1.body.user.id}`)
      .set('Authorization', `Bearer ${response1.body.token}`);

    expect(response2.status).toBe(204);
  });
});
