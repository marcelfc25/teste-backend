import request from 'supertest';
import { getConnection } from 'typeorm';

import UsersRepository from '../modules/users/infra/typeorm/repositories/UsersRepository';
import CreateAdminService from '../modules/users/services/CreateAdminService';
import app from '../shared/infra/http/app';
import createConnection from '../shared/infra/typeorm/index';

describe('Votes', () => {
  beforeAll(async () => {
    const connection = await createConnection();
    await connection.runMigrations();
  });

  afterAll(async () => {
    const connection = getConnection();
    await connection.dropDatabase();
    await connection.close();
  });

  it('should be able to vote in a movie', async () => {
    const usersRepository = new UsersRepository();
    const createAdmin = new CreateAdminService(usersRepository);
    await createAdmin.execute({
      name: 'user admin',
      email: 'vote@admin.com',
      password: '123456',
    });

    const response1 = await request(app).post('/sessions')
      .send({
        email: 'vote@admin.com',
        password: '123456',
      });

    const response2 = await request(app).post('/movies')
      .send({
        title: 'Homem de Ferro',
        original_title: 'Iron Man',
        duration: 206,
        rating: 12,
        release_year: 2008,
      })
      .set('Authorization', `Bearer ${response1.body.token}`);

    const response3 = await request(app).post('/votes')
      .send({
        movie_id: response2.body.id,
        grade: 4,
      })
      .set('Authorization', `Bearer ${response1.body.token}`);

    expect(response3.status).toBe(200);
    expect(response3.body).toEqual(
      expect.objectContaining({
        movie_id: response2.body.id,
        grade: 4,
      }),
    );
  });

  it('not should be able to vote in a movie with grade > 4', async () => {
    const usersRepository = new UsersRepository();
    const createAdmin = new CreateAdminService(usersRepository);
    await createAdmin.execute({
      name: 'user admin',
      email: 'vote2@admin.com',
      password: '123456',
    });

    const response1 = await request(app).post('/sessions')
      .send({
        email: 'vote2@admin.com',
        password: '123456',
      });

    const response2 = await request(app).post('/movies')
      .send({
        title: 'Homem de Ferro',
        original_title: 'Iron Man',
        duration: 206,
        rating: 12,
        release_year: 2008,
      })
      .set('Authorization', `Bearer ${response1.body.token}`);

    const response3 = await request(app).post('/votes')
      .send({
        movie_id: response2.body.id,
        grade: 5,
      })
      .set('Authorization', `Bearer ${response1.body.token}`);

    expect(response3.status).toBe(400);
  });

  it('not should be able to vote in a movie with grade < 0', async () => {
    const usersRepository = new UsersRepository();
    const createAdmin = new CreateAdminService(usersRepository);
    await createAdmin.execute({
      name: 'user admin',
      email: 'vote3@admin.com',
      password: '123456',
    });

    const response1 = await request(app).post('/sessions')
      .send({
        email: 'vote3@admin.com',
        password: '123456',
      });

    const response2 = await request(app).post('/movies')
      .send({
        title: 'Homem de Ferro',
        original_title: 'Iron Man',
        duration: 206,
        rating: 12,
        release_year: 2008,
      })
      .set('Authorization', `Bearer ${response1.body.token}`);

    const response3 = await request(app).post('/votes')
      .send({
        movie_id: response2.body.id,
        grade: -1,
      })
      .set('Authorization', `Bearer ${response1.body.token}`);

    expect(response3.status).toBe(400);
  });

  it('user does not should be able to vote in a movie more than once', async () => {
    const usersRepository = new UsersRepository();
    const createAdmin = new CreateAdminService(usersRepository);
    await createAdmin.execute({
      name: 'user admin',
      email: 'votemorethan@admin.com',
      password: '123456',
    });

    const response1 = await request(app).post('/sessions')
      .send({
        email: 'votemorethan@admin.com',
        password: '123456',
      });

    const response2 = await request(app).post('/movies')
      .send({
        title: 'Homem de Ferro',
        original_title: 'Iron Man',
        duration: 206,
        rating: 12,
        release_year: 2008,
      })
      .set('Authorization', `Bearer ${response1.body.token}`);

    const response3 = await request(app).post('/votes')
      .send({
        movie_id: response2.body.id,
        grade: 4,
      })
      .set('Authorization', `Bearer ${response1.body.token}`);

    const response4 = await request(app).post('/votes')
      .send({
        movie_id: response2.body.id,
        grade: 4,
      })
      .set('Authorization', `Bearer ${response1.body.token}`);

    expect(response4.status).toBe(400);
  });

  it('should be able to calculate avg votes in a movie', async () => {
    const usersRepository = new UsersRepository();
    const createAdmin = new CreateAdminService(usersRepository);
    await createAdmin.execute({
      name: 'user admin',
      email: 'vote_avg@admin.com',
      password: '123456',
    });

    const response1 = await request(app).post('/sessions')
      .send({
        email: 'vote_avg@admin.com',
        password: '123456',
      });

    const response2 = await request(app).post('/movies')
      .send({
        title: 'Homem de Ferro',
        original_title: 'Iron Man',
        duration: 206,
        rating: 12,
        release_year: 2008,
      })
      .set('Authorization', `Bearer ${response1.body.token}`);

    await request(app).post('/votes')
      .send({
        movie_id: response2.body.id,
        grade: 4,
      })
      .set('Authorization', `Bearer ${response1.body.token}`);

    await request(app).post('/users')
      .send({
        name: 'user 1',
        email: 'user1@gmail.com',
        password: '123456',
        password_confirmation: '123456',
      });

    const response5 = await request(app).post('/sessions')
      .send({
        email: 'user1@gmail.com',
        password: '123456',
      });

    await request(app).post('/votes')
      .send({
        movie_id: response2.body.id,
        grade: 3,
      })
      .set('Authorization', `Bearer ${response5.body.token}`);

    const response7 = await request(app).get(`/movies/${response2.body.id}`);

    expect(response7.status).toBe(200);
    expect(response7.body.average_votes).toEqual(
      3.5,
    );
  });
});
