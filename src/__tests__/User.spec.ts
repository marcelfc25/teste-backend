import request from 'supertest';
import { getConnection } from 'typeorm';

import app from '../shared/infra/http/app';
import createConnection from '../shared/infra/typeorm/index';

describe('Users', () => {
  beforeAll(async () => {
    const connection = await createConnection();
    await connection.runMigrations();
  });

  afterAll(async () => {
    const connection = getConnection();
    await connection.dropDatabase();
    await connection.close();
  });

  it('should be able to create a new user', async () => {
    const response = await request(app).post('/users')
      .send({
        name: 'user 1',
        email: 'user1@gmail.com',
        password: '123456',
        password_confirmation: '123456',
      });

    expect(response.status).toBe(200);
    expect(response.body).toEqual(
      expect.objectContaining({
        name: 'user 1',
        email: 'user1@gmail.com',
      }),
    );
  });

  it('not should be able to create a new user with e-mail already existing', async () => {
    await request(app).post('/users')
      .send({
        name: 'user 1',
        email: 'user1@gmail.com',
        password: '123456',
        password_confirmation: '123456',
      });

    const response2 = await request(app).post('/users')
      .send({
        name: 'user 2',
        email: 'user1@gmail.com',
        password: '123456',
        password_confirmation: '123456',
      });

    expect(response2.status).toBe(400);
  });

  it('should be able to update a user', async () => {
    await request(app).post('/users')
      .send({
        name: 'user 1',
        email: 'user1@gmail.com',
        password: '123456',
        password_confirmation: '123456',
      });

    const response1 = await request(app).post('/sessions')
      .send({
        email: 'user1@gmail.com',
        password: '123456',
      });

    const response2 = await request(app).put(`/users/${response1.body.user.id}`)
      .send({
        name: 'user 2',
        email: 'user1@gmail.com',
      })
      .set('Authorization', `Bearer ${response1.body.token}`);

    expect(response2.status).toBe(200);
    expect(response2.body).toEqual(
      expect.objectContaining({
        name: 'user 2',
        email: 'user1@gmail.com',
      }),
    );
  });

  it('should be able to delete a user', async () => {
    await request(app).post('/users')
      .send({
        name: 'user 1',
        email: 'user1@gmail.com',
        password: '123456',
        password_confirmation: '123456',
      });

    const response1 = await request(app).post('/sessions')
      .send({
        email: 'user1@gmail.com',
        password: '123456',
      });

    const response2 = await request(app).delete(`/users/${response1.body.user.id}`)
      .set('Authorization', `Bearer ${response1.body.token}`);

    expect(response2.status).toBe(204);
  });
});
