import request from 'supertest';
import { getConnection } from 'typeorm';

import UsersRepository from '../modules/users/infra/typeorm/repositories/UsersRepository';
import CreateAdminService from '../modules/users/services/CreateAdminService';
import app from '../shared/infra/http/app';
import createConnection from '../shared/infra/typeorm/index';

describe('Genres', () => {
  beforeAll(async () => {
    const connection = await createConnection();
    await connection.runMigrations();
  });

  afterAll(async () => {
    const connection = getConnection();
    await connection.dropDatabase();
    await connection.close();
  });

  it('should be able to create a new person', async () => {
    const usersRepository = new UsersRepository();
    const createAdmin = new CreateAdminService(usersRepository);
    await createAdmin.execute({
      name: 'user admin',
      email: 'admin3@admin.com',
      password: '123456',
    });

    const response1 = await request(app).post('/sessions')
      .send({
        email: 'admin3@admin.com',
        password: '123456',
      });

    const response2 = await request(app).post('/people')
      .send({
        name: 'Robert Downey Jr.',
        age: 55,
      })
      .set('Authorization', `Bearer ${response1.body.token}`);

    expect(response2.status).toBe(200);
    expect(response2.body).toEqual(
      expect.objectContaining({
        name: 'Robert Downey Jr.',
        age: 55,
      }),
    );
  });
});
