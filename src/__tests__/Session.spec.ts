import request from 'supertest';
import { getConnection } from 'typeorm';

import app from '../shared/infra/http/app';
import createConnection from '../shared/infra/typeorm/index';

describe('Sessions', () => {
  beforeAll(async () => {
    const connection = await createConnection();
    await connection.runMigrations();
  });

  afterAll(async () => {
    const connection = getConnection();
    await connection.dropDatabase();
    await connection.close();
  });

  it('should be able to create a new session', async () => {
    await request(app).post('/users')
      .send({
        name: 'user 1',
        email: 'user1@gmail.com',
        password: '123456',
        password_confirmation: '123456',
      });

    const response = await request(app).post('/sessions')
      .send({
        email: 'user1@gmail.com',
        password: '123456',
      });

    expect(response.status).toBe(200);
    expect(response.body.token).toBeDefined();
  });
});
