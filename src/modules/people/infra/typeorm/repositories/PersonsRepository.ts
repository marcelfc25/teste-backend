import {
  getRepository, Repository,
} from 'typeorm';

import ICreatePersonDTO from '../../../dtos/ICreatePersonDTO';
import IPersonsRepository from '../../../repositories/IPersonsRepository';
import Person from '../entities/Person';

class PersonsRepository implements IPersonsRepository {
      private ormRepository: Repository<Person>

      constructor() {
        this.ormRepository = getRepository(Person);
      }

      public async create({
        name,
        age,
      }: ICreatePersonDTO): Promise<Person> {
        const person = this.ormRepository.create({
          name,
          age,
        });
        await this.ormRepository.save(person);
        return person;
      }

      public async save(person: Person): Promise<Person> {
        return this.ormRepository.save(person);
      }

      public async findById(id: string): Promise<Person> {
        return this.ormRepository.findOne(id);
      }
}

export default PersonsRepository;
