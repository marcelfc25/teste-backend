import { Router } from 'express';

import adminAuthenticated from '../../../../users/infra/http/middlewares/adminAuthenticated';
import PeopleController from '../../controllers/PeopleController';
import personCreateValidation from '../middlewares/personCreateValidation';

const peopleRouter = Router();
const peopleController = new PeopleController();

peopleRouter.post('/', adminAuthenticated, personCreateValidation, peopleController.create);

export default peopleRouter;
