import { Request, Response, NextFunction } from 'express';
import Joi from 'joi';

import AppError from '../../../../../shared/errors/AppError';

export default async (request: Request, response: Response, next: NextFunction): Promise<any> => {
  const schema = Joi.object({
    name: Joi.string().required(),
    age: Joi.number().required(),
  });

  try {
    const {
      name,
      age,
    } = request.body;
    await schema.validateAsync({
      name,
      age,
    });
    next();
  } catch (err) {
    throw new AppError(err);
  }
};
