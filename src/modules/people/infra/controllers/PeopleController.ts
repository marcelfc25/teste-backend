import { classToClass } from 'class-transformer';
import { Request, Response } from 'express';

import CreatePersonService from '../../services/CreatePersonService';
import PersonsRepository from '../typeorm/repositories/PersonsRepository';

export default class PeopleController {
  public async index(request: Request, response: Response): Promise<Response> {
    return response.json({});
  }

  public async create(request: Request, response: Response): Promise<Response> {
    const personsRepository = new PersonsRepository();
    const {
      name,
      age,
    } = request.body;
    const createPerson = new CreatePersonService(
      personsRepository,
    );
    const person = await createPerson.execute({
      name,
      age,
    });
    return response.json(classToClass(person));
  }
}
