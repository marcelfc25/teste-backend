import Person from '../infra/typeorm/entities/Person';
import IPersonsRepository from '../repositories/IPersonsRepository';

interface IRequest {
    name: string;
    age: number;
}

class CreatePersonService {
  constructor(
        private personsRepository: IPersonsRepository,
  ) { }

  public async execute({
    name,
    age,
  }: IRequest): Promise<Person> {
    const person = await this.personsRepository.create({
      name,
      age,
    });

    return person;
  }
}

export default CreatePersonService;
