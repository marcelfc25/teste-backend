import ICreatePersonDTO from '../dtos/ICreatePersonDTO';
import Person from '../infra/typeorm/entities/Person';

export default interface IPersonsRepository {
    create(data: ICreatePersonDTO): Promise<Person>
    save(person: Person): Promise<Person>
    findById(id: string): Promise<Person>
};
