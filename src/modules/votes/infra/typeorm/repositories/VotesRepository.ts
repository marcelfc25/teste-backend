import {
  getRepository, Repository,
} from 'typeorm';

import ICreateVoteDTO from '../../../dtos/ICreateVoteDTO';
import IFindByUserAndMovieVoteDTO from '../../../dtos/IFindByUserAndMovieVoteDTO';
import IVotesRepository from '../../../repositories/IVotesRepository';
import Vote from '../entities/Vote';

class VotesRepository implements IVotesRepository {
      private ormRepository: Repository<Vote>

      constructor() {
        this.ormRepository = getRepository(Vote);
      }

      public async create({
        movie_id, grade, user_id,
      }: ICreateVoteDTO): Promise<Vote> {
        const vote = this.ormRepository.create({
          movie_id, grade, user_id,
        });
        await this.ormRepository.save(vote);
        return vote;
      }

      public async save(Vote: Vote): Promise<Vote> {
        return this.ormRepository.save(Vote);
      }

      public async findByUserAndMovie(
        { user_id, movie_id } : IFindByUserAndMovieVoteDTO,
      ): Promise<Vote | undefined> {
        const vote = await this.ormRepository.findOne({
          where: {
            user_id,
            movie_id,
          },
        });
        return vote;
      }
}

export default VotesRepository;
