import { Request, Response, NextFunction } from 'express';
import Joi from 'joi';

import AppError from '../../../../../shared/errors/AppError';

export default async (request: Request, response: Response, next: NextFunction): Promise<any> => {
  const schema = Joi.object({
    movie_id: Joi.string().required(),
    grade: Joi.number().min(0).max(4).required(),
  });

  try {
    const {
      movie_id, grade,
    } = request.body;
    await schema.validateAsync({
      movie_id, grade,
    });
    next();
  } catch (err) {
    throw new AppError(err);
  }
};
