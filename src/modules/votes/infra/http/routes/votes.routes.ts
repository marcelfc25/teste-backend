import { Router } from 'express';

import ensureAuthenticated from '../../../../users/infra/http/middlewares/ensureAuthenticated';
import VotesController from '../../controllers/VotesController';
import voteCreateValidation from '../middlewares/voteCreateValidation';

const votesRouter = Router();
const votesController = new VotesController();

votesRouter.post('/', ensureAuthenticated, voteCreateValidation, votesController.create);

export default votesRouter;
