import ICreateVoteDTO from '../dtos/ICreateVoteDTO';
import IFindByUserAndMovieVoteDTO from '../dtos/IFindByUserAndMovieVoteDTO';
import Vote from '../infra/typeorm/entities/Vote';

export default interface IVotesRepository {
    create(data: ICreateVoteDTO): Promise<Vote>
    save(vote: Vote): Promise<Vote>
    findByUserAndMovie(data: IFindByUserAndMovieVoteDTO): Promise<Vote | undefined>
}
