export default interface IFindByUserAndMovieVoteDTO {
    movie_id: string;
    user_id: string;
}
