import AppError from '../../../shared/errors/AppError';
import Vote from '../infra/typeorm/entities/Vote';
import IVotesRepository from '../repositories/IVotesRepository';

interface IRequest {
    movie_id: string;
    user_id: string;
    grade: number;
}

class CreateVoteService {
  constructor(
        private VotesRepository: IVotesRepository,
  ) { }

  public async execute({
    movie_id, user_id, grade,
  }: IRequest): Promise<Vote> {
    const existsVoteinMovieByUser = await this.VotesRepository.findByUserAndMovie(
      { movie_id, user_id },
    );

    if (existsVoteinMovieByUser) {
      throw new AppError('User has already voted in this movie');
    }

    const vote = await this.VotesRepository.create({
      movie_id, user_id, grade,
    });

    return vote;
  }
}

export default CreateVoteService;
