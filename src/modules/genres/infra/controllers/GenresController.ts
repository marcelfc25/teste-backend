import { classToClass } from 'class-transformer';
import { Request, Response } from 'express';

import CreateGenreService from '../../services/CreateGenreService';
import GenresRepository from '../typeorm/repositories/GenresRepository';

export default class MoviesController {
  public async index(request: Request, response: Response): Promise<Response> {
    return response.json({});
  }

  public async create(request: Request, response: Response): Promise<Response> {
    const genresRepository = new GenresRepository();
    const {
      title,
    } = request.body;
    const createMovie = new CreateGenreService(
      genresRepository,
    );
    const genre = await createMovie.execute({
      title,
    });
    return response.json(classToClass(genre));
  }
}
