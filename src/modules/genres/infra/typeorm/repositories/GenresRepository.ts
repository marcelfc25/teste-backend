import {
  getRepository, Repository,
} from 'typeorm';

import ICreateGenreDTO from '../../../dtos/ICreateGenreDTO';
import IGenresRepository from '../../../repositories/IGenresRepository';
import Genre from '../entities/Genre';

class GenresRepository implements IGenresRepository {
      private ormRepository: Repository<Genre>

      constructor() {
        this.ormRepository = getRepository(Genre);
      }

      public async create({
        title,
      }: ICreateGenreDTO): Promise<Genre> {
        const movie = this.ormRepository.create({
          title,
        });
        await this.ormRepository.save(movie);
        return movie;
      }

      public async save(genre: Genre): Promise<Genre> {
        return this.ormRepository.save(genre);
      }

      public async findById(id: string): Promise<Genre> {
        return this.ormRepository.findOne(id);
      }
}

export default GenresRepository;
