import { Request, Response, NextFunction } from 'express';
import Joi from 'joi';

import AppError from '../../../../../shared/errors/AppError';

export default async (request: Request, response: Response, next: NextFunction): Promise<any> => {
  const schema = Joi.object({
    title: Joi.string().required(),
  });

  try {
    const {
      title,
    } = request.body;
    await schema.validateAsync({
      title,
    });
    next();
  } catch (err) {
    throw new AppError(err);
  }
};
