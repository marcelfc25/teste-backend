import { Router } from 'express';

import adminAuthenticated from '../../../../users/infra/http/middlewares/adminAuthenticated';
import GenresController from '../../controllers/GenresController';
import genreCreateValidation from '../middlewares/genreCreateValidation';

const genresRouter = Router();
const genresController = new GenresController();

genresRouter.post('/', adminAuthenticated, genreCreateValidation, genresController.create);

export default genresRouter;
