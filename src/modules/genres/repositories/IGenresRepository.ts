import ICreateGenreDTO from '../dtos/ICreateGenreDTO';
import Genre from '../infra/typeorm/entities/Genre';

export default interface IGenresRepository {
    findById(id: string): Promise<Genre>
    create(data: ICreateGenreDTO): Promise<Genre>
    save(genre: Genre): Promise<Genre>
};
