import Genre from '../infra/typeorm/entities/Genre';
import IGenresRepository from '../repositories/IGenresRepository';

interface IRequest {
    title: string;
}

class CreateGenreService {
  constructor(
        private genresRepository: IGenresRepository,
  ) { }

  public async execute({
    title,
  }: IRequest): Promise<Genre> {
    const Genre = await this.genresRepository.create({
      title,
    });

    return Genre;
  }
}

export default CreateGenreService;
