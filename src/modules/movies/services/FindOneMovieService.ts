import Movie from '../infra/typeorm/entities/Movie';
import IMoviesRepository from '../repositories/IMoviesRepository';

class FindOneMovieService {
  constructor(
        private moviesRepository: IMoviesRepository,
  ) { }

  public async execute(id: string): Promise<Movie> {
    return this.moviesRepository.findById(id);
  }
}

export default FindOneMovieService;
