import AppError from '../../../shared/errors/AppError';
import IPersonsRepository from '../../people/repositories/IPersonsRepository';
import MovieDirector from '../infra/typeorm/entities/MovieDirector';
import IMoviesDirectorsRepository from '../repositories/IMoviesDirectorsRepository';
import IMoviesRepository from '../repositories/IMoviesRepository';

interface IRequest {
    movie_id: string;
    person_id: string;
}

class CreateMovieDirectorService {
  constructor(
        private moviesDirectorsRepository: IMoviesDirectorsRepository,
        private moviesRepository: IMoviesRepository,
        private personsRepository: IPersonsRepository,

  ) { }

  public async execute({
    movie_id, person_id,
  }: IRequest): Promise<MovieDirector> {
    const existsMovie = await this.moviesRepository.findById(movie_id);

    if (!existsMovie) {
      throw new AppError('Movie not found!');
    }

    const existsPerson = await this.personsRepository.findById(person_id);

    if (!existsPerson) {
      throw new AppError('Director not found!');
    }

    const movieDirector = await this.moviesDirectorsRepository.create({
      movie_id, person_id,
    });

    return movieDirector;
  }
}

export default CreateMovieDirectorService;
