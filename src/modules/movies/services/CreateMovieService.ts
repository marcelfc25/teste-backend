import Movie from '../infra/typeorm/entities/Movie';
import IMoviesRepository from '../repositories/IMoviesRepository';

interface IRequest {
    title: string;
    original_title: string;
    rating: number;
    release_year: number;
    duration: number;
}

class CreateMovieService {
  constructor(
        private moviesRepository: IMoviesRepository,
  ) { }

  public async execute({
    title, original_title, release_year, rating, duration,
  }: IRequest): Promise<Movie> {
    const movie = await this.moviesRepository.create({
      title, original_title, release_year, rating, duration,
    });

    return movie;
  }
}

export default CreateMovieService;
