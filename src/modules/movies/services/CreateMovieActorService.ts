import AppError from '../../../shared/errors/AppError';
import IPersonsRepository from '../../people/repositories/IPersonsRepository';
import MovieActor from '../infra/typeorm/entities/MovieActor';
import IMoviesActorsRepository from '../repositories/IMoviesActorsRepository';
import IMoviesRepository from '../repositories/IMoviesRepository';

interface IRequest {
    movie_id: string;
    person_id: string;
}

class CreateMovieActorService {
  constructor(
        private moviesActorsRepository: IMoviesActorsRepository,
        private moviesRepository: IMoviesRepository,
        private personsRepository: IPersonsRepository,

  ) { }

  public async execute({
    movie_id, person_id,
  }: IRequest): Promise<MovieActor> {
    const existsMovie = await this.moviesRepository.findById(movie_id);

    if (!existsMovie) {
      throw new AppError('Movie not found!');
    }

    const existsPerson = await this.personsRepository.findById(person_id);

    if (!existsPerson) {
      throw new AppError('Actor not found!');
    }

    const movieActor = await this.moviesActorsRepository.create({
      movie_id, person_id,
    });

    return movieActor;
  }
}

export default CreateMovieActorService;
