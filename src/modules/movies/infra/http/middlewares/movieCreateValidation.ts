import { Request, Response, NextFunction } from 'express';
import Joi from 'joi';

import AppError from '../../../../../shared/errors/AppError';

export default async (request: Request, response: Response, next: NextFunction): Promise<any> => {
  const schema = Joi.object({
    title: Joi.string().required(),
    original_title: Joi.string().required(),
    release_year: Joi.number().required(),
    rating: Joi.number().required(),
    duration: Joi.number().required(),
  });

  try {
    const {
      title, original_title, release_year, rating, duration,
    } = request.body;
    await schema.validateAsync({
      title, original_title, release_year, rating, duration,
    });
    next();
  } catch (err) {
    throw new AppError(err);
  }
};
