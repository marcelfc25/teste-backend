import { Request, Response, NextFunction } from 'express';
import Joi from 'joi';

import AppError from '../../../../../shared/errors/AppError';

export default async (request: Request, response: Response, next: NextFunction): Promise<any> => {
  const schema = Joi.object({
    person_id: Joi.string().required().uuid(),
    movie_id: Joi.string().required().uuid(),
  });

  try {
    const {
      person_id, movie_id,
    } = request.body;
    await schema.validateAsync({
      person_id, movie_id,
    });
    next();
  } catch (err) {
    throw new AppError(err);
  }
};
