import { Router } from 'express';

import adminAuthenticated from '../../../../users/infra/http/middlewares/adminAuthenticated';
import MoviesActorsController from '../../controllers/MoviesActorsController';
import MoviesController from '../../controllers/MoviesController';
import MoviesDirectorsController from '../../controllers/MoviesDirectorsController';
import MoviesGenresController from '../../controllers/MoviesGenresController';
import movieCreateValidation from '../middlewares/movieCreateValidation';
import movieGenreCreateValidation from '../middlewares/movieGenreCreateValidation';
import moviePersonCreateValidation from '../middlewares/moviePersonCreateValidation';

const moviesRouter = Router();
const moviesController = new MoviesController();
const moviesGenresController = new MoviesGenresController();
const moviesActorsController = new MoviesActorsController();
const moviesDirectorsController = new MoviesDirectorsController();

moviesRouter.post('/', adminAuthenticated, movieCreateValidation, moviesController.create);
moviesRouter.get('/:id', moviesController.show);
moviesRouter.get('/', moviesController.index);

moviesRouter.post('/movieGenre', adminAuthenticated, movieGenreCreateValidation, moviesGenresController.create);
moviesRouter.post('/movieActor', adminAuthenticated, moviePersonCreateValidation, moviesActorsController.create);
moviesRouter.post('/movieDirector', adminAuthenticated, moviePersonCreateValidation, moviesDirectorsController.create);

export default moviesRouter;
