import { classToClass } from 'class-transformer';
import { Request, Response } from 'express';

import PersonsRepository from '../../../people/infra/typeorm/repositories/PersonsRepository';
import CreateMovieDirectorService from '../../services/CreateMovieDirectorService';
import MoviesDirectorsRepository from '../typeorm/repositories/MoviesDirectorsRepository';
import MoviesRepository from '../typeorm/repositories/MoviesRepository';

export default class MoviesDirectorsController {
  public async create(request: Request, response: Response): Promise<Response> {
    const moviesDirectorsRepository = new MoviesDirectorsRepository();
    const moviesRepository = new MoviesRepository();
    const personsRepository = new PersonsRepository();
    const {
      movie_id, person_id,
    } = request.body;
    const createMovieDirectorService = new CreateMovieDirectorService(
      moviesDirectorsRepository,
      moviesRepository,
      personsRepository,
    );
    const movieDirector = await createMovieDirectorService.execute({
      movie_id, person_id,
    });
    return response.json(classToClass(movieDirector));
  }
}
