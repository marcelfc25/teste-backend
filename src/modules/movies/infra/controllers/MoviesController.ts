import { classToClass } from 'class-transformer';
import { Request, Response } from 'express';

import CreateMovieService from '../../services/CreateMovieService';
import FindMovieService from '../../services/FindMovieService';
import FindOneMovieService from '../../services/FindOneMovieService';
import MoviesRepository from '../typeorm/repositories/MoviesRepository';

export default class MoviesController {
  public async index(request: Request, response: Response): Promise<Response> {
    const moviesRepository = new MoviesRepository();
    const {
      title, director, actor, genre,
    } = request.query;

    const findMovieService = new FindMovieService(moviesRepository);
    const movies = await findMovieService.execute({
      title: title ? String(title) : undefined,
      director: director ? String(director) : undefined,
      actor: actor ? String(actor) : undefined,
      genre: genre ? String(genre) : undefined,
    });

    return response.json(classToClass(movies));
  }

  public async create(request: Request, response: Response): Promise<Response> {
    const moviesRepository = new MoviesRepository();
    const {
      title, original_title, release_year, rating, duration,
    } = request.body;
    const createMovie = new CreateMovieService(
      moviesRepository,
    );
    const movie = await createMovie.execute({
      title, original_title, release_year, rating, duration,
    });
    return response.json(classToClass(movie));
  }

  public async show(request: Request, response: Response): Promise<Response> {
    const moviesRepository = new MoviesRepository();
    const { id } = request.params;
    const findOneMovieService = new FindOneMovieService(moviesRepository);
    const movie = await findOneMovieService.execute(id);
    return response.json(classToClass(movie));
  }
}
