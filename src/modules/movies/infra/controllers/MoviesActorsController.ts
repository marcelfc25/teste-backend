import { classToClass } from 'class-transformer';
import { Request, Response } from 'express';

import PersonsRepository from '../../../people/infra/typeorm/repositories/PersonsRepository';
import CreateMovieActorService from '../../services/CreateMovieActorService';
import MoviesActorsRepository from '../typeorm/repositories/MoviesActorsRepository';
import MoviesRepository from '../typeorm/repositories/MoviesRepository';

export default class MoviesActorsController {
  public async create(request: Request, response: Response): Promise<Response> {
    const moviesActorsRepository = new MoviesActorsRepository();
    const moviesRepository = new MoviesRepository();
    const personsRepository = new PersonsRepository();
    const {
      movie_id, person_id,
    } = request.body;
    const createMovieActorService = new CreateMovieActorService(
      moviesActorsRepository,
      moviesRepository,
      personsRepository,
    );
    const movieActor = await createMovieActorService.execute({
      movie_id, person_id,
    });
    return response.json(classToClass(movieActor));
  }
}
