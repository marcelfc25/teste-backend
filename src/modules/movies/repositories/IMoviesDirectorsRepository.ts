import ICreateMovieDirectorDTO from '../dtos/ICreateMovieDirectorDTO';
import MovieDirector from '../infra/typeorm/entities/MovieDirector';

export default interface IMoviesDirectorsRepository {
    create(data: ICreateMovieDirectorDTO): Promise<MovieDirector>
    save(movieDirector: MovieDirector): Promise<MovieDirector>
};
