import ICreateMovieGenreDTO from '../dtos/ICreateMovieGenreDTO';
import MovieGenre from '../infra/typeorm/entities/MovieGenre';

export default interface IMoviesGenresRepository {
    create(data: ICreateMovieGenreDTO): Promise<MovieGenre>
    save(movieGenre: MovieGenre): Promise<MovieGenre>
};
