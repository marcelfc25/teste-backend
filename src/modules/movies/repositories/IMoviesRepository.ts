import ICreateMovieDTO from '../dtos/ICreateMovieDTO';
import ISearchMoviesDTO from '../dtos/ISearchMoviesDTO';
import Movie from '../infra/typeorm/entities/Movie';

export default interface IMoviesRepository {
    find(data: ISearchMoviesDTO): Promise<Movie[] | undefined>
    findById(id: string): Promise<Movie | undefined>
    create(data: ICreateMovieDTO): Promise<Movie>
    save(movie: Movie): Promise<Movie>
};
