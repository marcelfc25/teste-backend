import ICreateMovieActorDTO from '../dtos/ICreateMovieActorDTO';
import MovieActor from '../infra/typeorm/entities/MovieActor';

export default interface IMoviesActorsRepository {
    create(data: ICreateMovieActorDTO): Promise<MovieActor>
    save(movieActor: MovieActor): Promise<MovieActor>
};
