import AppError from '../../../shared/errors/AppError';
import User from '../infra/typeorm/entities/User';
import IUsersRepository from '../repositories/IUsersRepository';

interface IRequest {
    id: string;
    name: string;
    email: string;
}

class UpdateUserService {
  constructor(
        private usersRepository: IUsersRepository,
  ) { }

  public async execute({
    id, name, email,
  }: IRequest): Promise<User> {
    const checkUserExists = await this.usersRepository.findById(id);

    if (!checkUserExists) {
      throw new AppError('User not found');
    }

    const checkUserEmail = await this.usersRepository.findByEmail(email);

    if (checkUserEmail && checkUserEmail.id !== id) {
      throw new AppError('E-mail already in use');
    }

    const user = await this.usersRepository.update({
      id,
      name,
      email,
    });

    return user;
  }
}

export default UpdateUserService;
