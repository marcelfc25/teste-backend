import AppError from '../../../shared/errors/AppError';
import User from '../infra/typeorm/entities/User';
import IUsersRepository from '../repositories/IUsersRepository';

class DeleteUserService {
  constructor(
        private usersRepository: IUsersRepository,
  ) { }

  public async execute(id: string): Promise<User> {
    const checkUserExists = await this.usersRepository.findById(id);

    if (!checkUserExists) {
      throw new AppError('Usuário não encontrado');
    }

    return this.usersRepository.delete(id);
  }
}

export default DeleteUserService;
