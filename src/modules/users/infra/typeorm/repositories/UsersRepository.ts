import { format } from 'date-fns';
import {
  getRepository, Repository, Not, IsNull,
} from 'typeorm';

import ICreateUserDTO from '../../../dtos/ICreateUserDTO';
import IUpdateUserDTO from '../../../dtos/IUpdateUserDTO';
import IUsersRepository from '../../../repositories/IUsersRepository';
import User from '../entities/User';

class UsersRepository implements IUsersRepository {
    private ormRepository: Repository<User>

    constructor() {
      this.ormRepository = getRepository(User);
    }

    public async findById(id: string): Promise<User | undefined> {
      const user = this.ormRepository.findOne({
        where: {
          id,
          deleted_at: IsNull(),
        },
      });
      return user;
    }

    public async findByEmail(email: string): Promise<User | undefined> {
      const user = this.ormRepository.findOne({
        where: {
          email,
          deleted_at: IsNull(),
        },
      });
      return user;
    }

    public async create({
      name, email, role, password,
    }: ICreateUserDTO): Promise<User> {
      const user = this.ormRepository.create({
        name, email, role, password,
      });
      await this.ormRepository.save(user);
      return user;
    }

    public async update({
      id, name, email,
    }: IUpdateUserDTO): Promise<User> {
      return this.ormRepository.save({
        id,
        name,
        email,
      });
    }

    public async delete(id: string): Promise<User> {
      return this.ormRepository.save({
        id,
        deleted_at: format(new Date(), 'yyyy-MM-dd'),
      });
    }

    public async save(user: User): Promise<User> {
      return this.ormRepository.save(user);
    }
}

export default UsersRepository;
