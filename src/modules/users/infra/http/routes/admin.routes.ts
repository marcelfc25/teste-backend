import { Router } from 'express';

import AdminController from '../../controllers/AdminController';
import adminAuthenticated from '../middlewares/adminAuthenticated';
import userCreateValidation from '../middlewares/userCreateValidation';
import userDeleteValidation from '../middlewares/userDeleteValidation';
import userUpdateValidation from '../middlewares/userUpdateValidation';

const adminRouter = Router();
const adminController = new AdminController();

adminRouter.post('/', adminAuthenticated, userCreateValidation, adminController.create);
adminRouter.put('/:id', adminAuthenticated, userUpdateValidation, adminController.update);
adminRouter.delete('/:id', adminAuthenticated, userDeleteValidation, adminController.delete);

export default adminRouter;
