import { Router } from 'express';

import SessionsController from '../../controllers/SessionsController';
import sessionCreateValidation from '../middlewares/sessionCreateValidation';

const sessionsRouter = Router();
const sessionsController = new SessionsController();

sessionsRouter.post('/', sessionCreateValidation, sessionsController.create);

export default sessionsRouter;
