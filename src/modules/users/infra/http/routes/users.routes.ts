import { Router } from 'express';

import UsersController from '../../controllers/UsersController';
import userCreateValidation from '../middlewares/userCreateValidation';
import userDeleteValidation from '../middlewares/userDeleteValidation';
import userUpdateValidation from '../middlewares/userUpdateValidation';

const usersRouter = Router();
const usersController = new UsersController();

usersRouter.post('/', userCreateValidation, usersController.create);
usersRouter.put('/:id', userUpdateValidation, usersController.update);
usersRouter.delete('/:id', userDeleteValidation, usersController.delete);

export default usersRouter;
