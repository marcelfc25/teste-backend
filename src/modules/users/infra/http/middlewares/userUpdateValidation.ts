import { Request, Response, NextFunction } from 'express';
import Joi from 'joi';

import AppError from '../../../../../shared/errors/AppError';

export default async (request: Request, response: Response, next: NextFunction): Promise<any> => {
  const schema = Joi.object({
    name: Joi.string().required(),
    id: Joi.string().required().uuid(),
    email: Joi.string().required().email(),
  });

  try {
    const { name, email } = request.body;
    const { id } = request.params;
    await schema.validateAsync({
      name, email, id,
    });
    next();
  } catch (err) {
    throw new AppError(err);
  }
};
