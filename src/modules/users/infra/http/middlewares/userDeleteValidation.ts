import { Request, Response, NextFunction } from 'express';
import Joi from 'joi';

import AppError from '../../../../../shared/errors/AppError';

export default async (request: Request, response: Response, next: NextFunction): Promise<any> => {
  const schema = Joi.object({
    id: Joi.string().required().uuid(),
  });

  try {
    const { id } = request.params;
    await schema.validateAsync({
      id,
    });
    next();
  } catch (err) {
    throw new AppError(err);
  }
};
