import { Request, Response, NextFunction } from 'express';
import Joi from 'joi';

import AppError from '../../../../../shared/errors/AppError';

export default async (request: Request, response: Response, next: NextFunction): Promise<any> => {
  const schema = Joi.object({
    name: Joi.string().required(),
    email: Joi.string().required().email(),
    password: Joi.string().required().min(6),
    password_confirmation: Joi.any().valid(Joi.ref('password')).required(),
  });

  try {
    const {
      name, email, password, password_confirmation,
    } = request.body;
    await schema.validateAsync({
      name, email, password, password_confirmation,
    });
    next();
  } catch (err) {
    throw new AppError(err);
  }
};
