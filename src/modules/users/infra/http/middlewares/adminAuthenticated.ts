import { Request, Response, NextFunction } from 'express';
import { verify } from 'jsonwebtoken';

import authConfig from '../../../../../config/auth';
import AppError from '../../../../../shared/errors/AppError';
import UsersRepository from '../../typeorm/repositories/UsersRepository';

interface ITokenPayload {
    iat: number,
    exp: number,
    sub: string
}

export default async function adminAuthenticated(
  request: Request, response: Response, next: NextFunction,
): Promise<void> {
  const authHeader = request.headers.authorization;

  if (!authHeader) {
    throw new AppError('JWT token is missing!', 401);
  }

  const [, token] = authHeader.split(' ');
  try {
    const decoded = verify(token, authConfig.jwt.secret);
    const { sub } = decoded as ITokenPayload;
    const usersRepository = new UsersRepository();
    const user = await usersRepository.findById(sub);

    if (user) {
      if (user.role !== 'admin') {
        throw new AppError('User without permission!', 401);
      }
      request.user = {
        id: sub,
      };
      next();
    } else {
      throw new AppError('User not found!', 401);
    }
  } catch (err) {
    throw new AppError('User without permission!', 401);
  }
}
