import { Request, Response, NextFunction } from 'express';
import Joi from 'joi';

import AppError from '../../../../../shared/errors/AppError';

export default async (request: Request, response: Response, next: NextFunction): Promise<any> => {
  const schema = Joi.object({
    email: Joi.string().required().email(),
    password: Joi.string().required().min(6),
  });

  try {
    const {
      email, password,
    } = request.body;
    await schema.validateAsync({
      email, password,
    });
    next();
  } catch (err) {
    throw new AppError(err);
  }
};
