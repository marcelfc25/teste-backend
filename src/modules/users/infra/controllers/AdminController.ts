import { classToClass } from 'class-transformer';
import { Request, Response } from 'express';

import CreateAdminService from '../../services/CreateAdminService';
import DeleteAdminService from '../../services/DeleteAdminService';
import UpdateAdminService from '../../services/UpdateAdminService';
import UsersRepository from '../typeorm/repositories/UsersRepository';

export default class AdminController {
  public async create(request: Request, response: Response): Promise<Response> {
    const usersRepository = new UsersRepository();
    const {
      name, email, password,
    } = request.body;
    const createAdmin = new CreateAdminService(
      usersRepository,
    );
    const admin = await createAdmin.execute({
      name, email, password,
    });
    return response.json(classToClass(admin));
  }

  public async update(request: Request, response: Response): Promise<Response> {
    const usersRepository = new UsersRepository();
    const { id } = request.params;
    const {
      name, email,
    } = request.body;
    const updateUser = new UpdateAdminService(
      usersRepository,
    );
    const user = await updateUser.execute({
      id, name, email,
    });
    return response.json(classToClass(user));
  }

  public async delete(request: Request, response: Response): Promise<Response> {
    const usersRepository = new UsersRepository();
    const { id } = request.params;
    const deleteAdmin = new DeleteAdminService(
      usersRepository,
    );
    await deleteAdmin.execute(id);
    return response.status(204).send();
  }
}
