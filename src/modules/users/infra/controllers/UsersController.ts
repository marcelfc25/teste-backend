import { classToClass } from 'class-transformer';
import { Request, Response } from 'express';

import CreateUserService from '../../services/CreateUserService';
import DeleteUserService from '../../services/DeleteUserService';
import UpdateUserService from '../../services/UpdateUserService';
import UsersRepository from '../typeorm/repositories/UsersRepository';

export default class UsersController {
  public async create(request: Request, response: Response): Promise<Response> {
    const usersRepository = new UsersRepository();
    const {
      name, email, password,
    } = request.body;
    const createUser = new CreateUserService(
      usersRepository,
    );
    const user = await createUser.execute({
      name, email, password,
    });
    return response.json(classToClass(user));
  }

  public async update(request: Request, response: Response): Promise<Response> {
    const usersRepository = new UsersRepository();
    const { id } = request.params;
    const {
      name, email,
    } = request.body;
    const updateUser = new UpdateUserService(
      usersRepository,
    );
    const user = await updateUser.execute({
      id, name, email,
    });
    return response.json(classToClass(user));
  }

  public async delete(request: Request, response: Response): Promise<Response> {
    const usersRepository = new UsersRepository();
    const { id } = request.params;
    const deleteUser = new DeleteUserService(
      usersRepository,
    );
    await deleteUser.execute(id);
    return response.status(204).send();
  }
}
