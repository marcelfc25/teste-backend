import { Router } from 'express';

import genresRoute from '../../../../modules/genres/infra/http/routes/genres.routes';
import moviesRoute from '../../../../modules/movies/infra/http/routes/movies.routes';
import peopleRoute from '../../../../modules/people/infra/http/routes/people.routes';
import adminRoute from '../../../../modules/users/infra/http/routes/admin.routes';
import sessionsRoute from '../../../../modules/users/infra/http/routes/sessions.routes';
import usersRoute from '../../../../modules/users/infra/http/routes/users.routes';
import votesRoute from '../../../../modules/votes/infra/http/routes/votes.routes';

const routes = Router();

routes.use('/sessions', sessionsRoute);
routes.use('/users', usersRoute);
routes.use('/admin', adminRoute);
routes.use('/movies', moviesRoute);
routes.use('/genres', genresRoute);
routes.use('/people', peopleRoute);
routes.use('/votes', votesRoute);

export default routes;
